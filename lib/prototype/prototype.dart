// Mixin to enable cloning capability
mixin Cloneable<T> {
  T clone();
}

class Shape with Cloneable<Shape>{
  String type;

  Shape(this.type);

  @override
  Shape clone() {
    return Shape(type);
  }
}