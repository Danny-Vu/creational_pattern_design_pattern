// Abstract products
abstract class Chair {
  void sitOn();
  void hasLegs();
}

abstract class Table {
  void useFor();
}

// Concrete products: Victorian style furniture
class VictorianChair implements Chair {
  @override
  void sitOn() {
    print('Sitting on a Victorian chair');
  }

  @override
  void hasLegs() {
    // TODO: implement hasLegs
  }
}

class VictorianTable implements Table {
  @override
  void useFor() {
    print('Using a Victorian table');
  }
}

// Concrete products: Modern style furniture
class ModernChair implements Chair {
  @override
  void sitOn() {
    print('Sitting on a Modern chair');
  }

  @override
  void hasLegs() {
    // TODO: implement hasLegs
  }
}

class ModernTable implements Table {
  @override
  void useFor() {
    print('Using a Modern table');
  }
}

// Abstract factory
abstract class FurnitureFactory {
  Chair createChair();
  Table createTable();
}

// Concrete factories
class VictorianFurnitureFactory implements FurnitureFactory {
  @override
  Chair createChair() {
    return VictorianChair();
  }

  @override
  Table createTable() {
    return VictorianTable();
  }
}

class ModernFurnitureFactory implements FurnitureFactory {
  @override
  Chair createChair() {
    return ModernChair();
  }

  @override
  Table createTable() {
    return ModernTable();
  }
}

