class Singleton {
  // Private constructor
  Singleton._privateConstructor();

  static final Singleton _instance = Singleton._privateConstructor();

  static Singleton get instance => _instance;

  void doSomething() {
    print('Singleton instance is doing something');
  }

}