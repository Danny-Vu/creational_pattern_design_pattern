// Product: Sandwich
class Sandwich {
   String bread;
   String meat;
   List<String> veggies;
   List<String> sauces;

  Sandwich(this.bread, this.meat, this.veggies, this.sauces);

  void describe() {
    print('Sandwich with $bread bread, $meat,');
    print('Veggies: $veggies');
    print('Sauces: $sauces');
  }
}

// Abstract builder
abstract class SandwichBuilder {
  void addBread();
  void addMeat();
  void addVeggies();
  void addSauces();
  Sandwich getSandwich();
}

// Concrete builder: Italian Sandwich
class ItalianSandwichBuilder implements SandwichBuilder {
  final Sandwich _sandwich = Sandwich('Italian bread', 'Salami', [], []);


  @override
  void addBread() {
    _sandwich.bread = 'Italian bread';
  }

  @override
  void addMeat() {
    _sandwich.meat = 'Salami';
  }

  @override
  void addVeggies() {
    _sandwich.veggies = ['Lettuce', 'Tomato', 'Onion'];
  }

  @override
  void addSauces() {
    _sandwich.sauces = ['Mayo', 'Mustard'];
  }

  @override
  Sandwich getSandwich() {
    return _sandwich;
  }
}

// Director
class SandwichMaker {
  SandwichBuilder _builder;

  SandwichMaker(this._builder);

  Sandwich makeSandwich() {
    _builder.addBread();
    _builder.addMeat();
    _builder.addVeggies();
    _builder.addSauces();
    return _builder.getSandwich();
  }
}