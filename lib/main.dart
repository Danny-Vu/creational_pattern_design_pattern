import 'package:creational_pattern_design_pattern/abstract_factory/abstract_factory.dart';
import 'package:creational_pattern_design_pattern/builder/builder.dart';
import 'package:creational_pattern_design_pattern/factory_method/factory_method.dart';
import 'package:creational_pattern_design_pattern/prototype/prototype.dart';
import 'package:creational_pattern_design_pattern/singleton/singleton.dart';


void main() {
  /// Factory Method
  VehicleFactory carFactory = CarFactory();
  Vehicle car = carFactory.createVehicle();
  car.drive(); // Output: Driving a car

  VehicleFactory motorcycleFactory = MotorcycleFactory();
  Vehicle motorcycle = motorcycleFactory.createVehicle();
  motorcycle.drive(); // Output: Riding a motorcycle

  /// Abstract Factory
  // Using the Victorian furniture factory
  FurnitureFactory victorianFactory = VictorianFurnitureFactory();
  Chair victorianChair = victorianFactory.createChair();
  Table victorianTable = victorianFactory.createTable();

  victorianChair.sitOn(); // Output: Sitting on a Victorian chair
  victorianTable.useFor(); // Output: Using a Victorian table

  // Using the Modern furniture factory
  FurnitureFactory modernFactory = ModernFurnitureFactory();
  Chair modernChair = modernFactory.createChair();
  Table modernTable = modernFactory.createTable();

  modernChair.sitOn(); // Output: Sitting on a Modern chair
  modernTable.useFor(); // Output: Using a Modern table

  /// Builder
  // Using the Italian sandwich builder
  var italianBuilder = ItalianSandwichBuilder();
  var sandwichMaker = SandwichMaker(italianBuilder);

  var italianSandwich = sandwichMaker.makeSandwich();
  italianSandwich.describe();

  /// Prototype
  // Create a prototype object
  var prototypeShape = Shape('Circle');

  // Clone the prototype to create new object
  var shape1 = prototypeShape.clone();
  var shape2 = prototypeShape.clone();

  // Output the types of the cloned shapes
  shape1.type; // Output: Circle
   shape2.type; // Output: Circle

  /// Singleton
  // Accessing the Singleton instance
  var singleton1 = Singleton.instance;
  var singleton2 = Singleton.instance;

  // Both instances should be the same
  print(identical(singleton1, singleton2)); // Output: true

  // Using the Singleton instance
  singleton1.doSomething(); // Output: Singleton instance is doing something.
  singleton2.doSomething(); // Output: Singleton instance is doing something.
}






