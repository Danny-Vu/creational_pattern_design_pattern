// Define a common interface for vehicles
abstract class Vehicle {
  void drive();
}

// Implement different types of vehicles
class Car implements Vehicle {
  @override
  void drive() {
    print('Driving a car');
  }
}

class Motorcycle implements Vehicle {
  @override
  void drive() {
    print('Riding a motorcycle');
  }
}

// Create a VehicleFactory interface
abstract class VehicleFactory {
  Vehicle createVehicle();
}

// Implement concrete factories for different vehicle types
class CarFactory implements VehicleFactory {
  @override
  Vehicle createVehicle() {
    return Car();
  }
}

class MotorcycleFactory implements VehicleFactory {
  @override
  Vehicle createVehicle() {
    return Motorcycle();
  }
}